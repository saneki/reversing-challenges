# Solutions

These are my solutions for the `v6.04` recruiting card challenges. Enjoy! 😄

## Brainfuck Sample

This challenge is seemingly just a program written in Brainfuck, which outputs the flag.

Answer: `Flag?:Pardon`

## BCD Integers

This challenge starts with disassembling some intel instructions. These instructions use `fild` and `fbstp` to take two 64-bit integers, and encode them in packed BCD (binary-coded decimal) format.

```
0x0: fild qword ptr [rax]
0x2: fbstp tbyte ptr [rip + 0xa]
0x8: fild qword ptr [rax + 8]
0xb: fbstp tbyte ptr [rip + 0xa]
0x11: int3
```

The output should contain 10 bytes for each input. However, only the first 9 bytes of each are provided. I just used `0x00` as the final byte for each, which seemed to work fine.

```
Output1 = 0x46, 0x38, 0x88, 0x15, 0x78, 0x56, 0x54, 0x85, 0x02
Output2 = 0x54, 0x40, 0x10, 0x00, 0x11, 0x25, 0x14, 0x92, 0x01
```

The idea is to figure out what (input) data was stored at `rax` which would generate these outputs.

To do so, I wrote a small NASM script which uses inverse instructions `fbld` and `fistp`. Using these instructions, each packed BCD output is converted into a real and pushed onto the floating register stack, before being stored into memory as a 64-bit value.

You may then run the program in a debugger and check memory for the flag.

Answer: `Flag:reversBCD`

## QR Code

The QR code decodes to a hex string, which evaluates to `0xD5` bytes.

```
00000000: EB 3F 41 58 6A 00 48 89  E6 48 C7 C7 01 00 00 00  .?AXj.H..H......
00000010: 48 C7 C2 01 00 00 00 43  8A 04 08 3C 00 74 12 30  H......C...<.t.0
00000020: D8 88 06 48 C7 C0 01 00  00 00 0F 05 49 FF C1 EB  ...H........I...
00000030: E6 48 C7 C0 3C 00 00 00  48 C7 C7 00 00 00 00 0F  .H..<...H.......
00000040: 05 E8 BC FF FF FF CC EE  F3 E4 A1 C8 EF F2 F5 F3  ................
00000050: F4 E2 F5 E8 EE EF F2 A1  D1 ED E4 E0 F2 E4 AD A1  ................
00000060: D2 E8 F3 BB 89 81 81 92  81 81 81 81 82 61 C1 A4  .............a..
00000070: A2 3C 7E 7D A2 24 81 82  A5 85 81 80 A5 87 81 80  .<~}.$..........
00000080: 10 88 81 81 90 A1 81 86  81 81 81 81 80 B1 C9 A7  ................
00000090: 2E 28 81 81 A5 83 8E 25  81 81 81 8D 89 81 81 86  .(.....%........
000000A0: A0 89 81 80 A5 83 8E 20  A5 85 81 81 81 81 81 8D  ....... ........
000000B0: 8D 81 81 83 81 81 81 81  DF F5 F8 FE A3 F8 EB FA  ................
000000C0: F1 F0 ED FC FA ED 81 81  81 81 81 81 81 81 81 81  ................
000000D0: 81 81 81 81 00
```

Running `file` on this data yields the following message:

```
DOS executable (COM), start instruction 0xeb3f4158 6a004889
```

This implies we may want to try interpreting the data as 16-bit intel instructions.

```
0x0: jmp 0x41
0x2: inc cx
<somewhat-sensible 16-bit intel instructions...>
0x41: call 0
0x43: ...
```

It jumps immediately to offset `0x43`. However, at that offset the data becomes very different. If we XOR by `0x81` starting at offset `0x43`, we see an interesting string:

```
00000000: 7E 7E 7E 4D 6F 72 65 20  49 6E 73 74 72 75 63 74  ~~~More Instruct
00000010: 69 6F 6E 73 20 50 6C 65  61 73 65 2C 20 53 69 72  ions Please, Sir
00000020: 3A 08 00 00 13 00 00 00  00 03 E0 40 25 23 BD FF  :..........@%#..
00000030: FC 23 A5 00 03 24 04 00  01 24 06 00 01 91 09 00  .#...$...$......
00000040: 00 11 20 00 07 00 00 00  00 01 30 48 26 AF A9 00  .. .......0H&...
00000050: 00 24 02 0F A4 00 00 00  0C 08 00 00 07 21 08 00  .$...........!..
00000060: 01 24 02 0F A1 24 04 00  00 00 00 00 0C 0C 00 00  .$...$..........
00000070: 02 00 00 00 00 5E 74 79  7F 22 79 6A 7B 70 71 6C  .....^ty."yj{pql
00000080: 7D 7B 6C 00 00 00 00 00  00 00 00 00 00 00 00 00  }{l.............
00000090: 00 81
```

Cleaning this data up a bit more, we can ignore the string and the tail `0x81` byte (which previously looked like a null byte):

```
00000000: 08 00 00 13 00 00 00 00  03 E0 40 25 23 BD FF FC  ..........@%#...
00000010: 23 A5 00 03 24 04 00 01  24 06 00 01 91 09 00 00  #...$...$.......
00000020: 11 20 00 07 00 00 00 00  01 30 48 26 AF A9 00 00  . .......0H&....
00000030: 24 02 0F A4 00 00 00 0C  08 00 00 07 21 08 00 01  $...........!...
00000040: 24 02 0F A1 24 04 00 00  00 00 00 0C 0C 00 00 02  $...$...........
00000050: 00 00 00 00 5E 74 79 7F  22 79 6A 7B 70 71 6C 7D  ....^ty."yj{pql}
00000060: 7B 6C 00 00 00 00 00 00  00 00 00 00 00 00 00 00  {l..............
```

This almost looks like RISC instructions! However, I wasn't able to determine which architecture they belonged to, if any.

Instead, I focused on the data near the end which looks like a weird string. XOR-ing the first four bytes by `b'Flag'` yielded `b'\x18\x18\x18\x18'`. When XOR-ing each byte by `0x18`, it reveals the flag.

Answer: `Flag:architect`

## Remainder

This challenge starts with some intel assembly, and 6 integers.

```s
_start:
    xor ecx, ecx
    inc ecx
    jmp label_0x24
label_0x06:
    pop rsi
    mov ebx, dword [rsi]
label_0x09:
    mov eax, dword [rdi+rcx*4-4]
    mov edx, dword [rsi|rcx*4]
    mul rdx
    div rbx
    dec rdx
    jne label_0x23
    inc ecx
    cmp ecx, 5
    jl label_0x09
    ret
label_0x23:
    hlt
label_0x24:
    call label_0x06
```

This implies that 32-bit integers are going to be pulled from two locations: `rsi` and `rdi`.

As the `rcx` loop occurs 5 times, `rsi` will load 6 integers and `rdi` will load 5. This implies `rsi` is a good candidate for the list of integers that we are given:

```py
[0x79BC1667, 0x77DD9E7E, 0x5CE898F5, 0x72218E79, 0x3C521B22, 0]
```

Thus, `ebx` is always equal to `0x79BC1667`, while `edx` will iterate through the following integers.

The primary idea seems to be passing the `jne label_0x23` check. This instruction may branch to a `hlt` instruction, which is reserved for ring 0 and would likely segfault in userland.

To prevent jumping, `dec rdx` must set the Z flag to 1. Thus, `rdx == 1` is required.

The primary logic occurring per loop may be summarized as:

```
mul rdx  -> rdx:rax = rax * rdx
div rbx  -> (rax, rdx) = rdx:rax / rbx
```

This means that `rdx` holds the remainder from dividing the `mul` result by `rbx`. Thus we have the formula: `(rax * rdx) == (rbx * N) + 1`

Simplified formula: `(rax * rdx) % rbx == 1`

Given our inputs for `rbx` and `rdx`, we can plug this into a solver like Z3! However, we will ignore the final `rdx` value of 0 as it doesn't make much sense to solve.

Doing so gives us the corresponding `rax` values: `[0x67616c46, 0x6d65723a, 0x646e6961, 0x7265]`

As expected, this is ASCII which spells out the flag.

Answer: `Flag:remainder`
