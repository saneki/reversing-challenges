; To assemble & compile for Win64 executable:
; nasm -f win64 bcd.asm
; link /ENTRY:_start /SUBSYSTEM:CONSOLE bcd.obj

SECTION .data

input_0 db 46h, 38h, 88h, 15h, 78h, 56h, 54h, 85h, 02h, 00h
input_1 db 54h, 40h, 10h, 00h, 11h, 25h, 14h, 92h, 01h, 00h

SECTION .bss

output_0: RESQ 1
output_1: RESQ 1

SECTION .text

global _start

_start:
    mov rax, input_0
    fbld tword [rax]
    mov rbx, output_0
    fistp qword [rbx]

    mov rax, input_1
    fbld tword [rax]
    mov rbx, output_1
    fistp qword [rbx]

    int3
