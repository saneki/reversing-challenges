#!/usr/bin/env python

from binascii import unhexlify
from capstone import Cs, CS_ARCH_X86, CS_MODE_16, CS_MODE_64
from contextlib import contextmanager
import hexdump
from itertools import takewhile
from result import Result, Ok, Err
import struct
from typing import Generator, Sequence
from z3 import Int, ModelRef, Solver

def disasm(md: Cs, data: bytes, offset: int = 0):
    for instr in md.disasm(data, offset=offset):
        print('0x%x: %s %s' % (instr.address, instr.mnemonic, instr.op_str))

@contextmanager
def section(title: str, count: int = 40) -> Generator[None, None, None]:
    try:
        print('\n--- ' + title + ' ' + '-' * (count - len(title)) + '\n')
        yield
    finally:
        ...

message = unhexlify(f'{int("J7YE0X3QNCD091YZPRITBB13BCLPYIGUWC2GD", 36):x}').decode()
assert message == 'Send to ctf@raytheon.com'

brainfuck_sample = '++++3++++2++[>b+>++j+>+++t++++ >+++' + \
                   'R++++C+++<4<<<-]:>>>. >+++2++++A+.-' + \
                   '-E-----2----5.+++4+++.2<---8----4 .' + \
                   '-----3.+++3++++D++++F+++++C++++E++.' + \
                   '>6----1--.+3+++++C++++S++++2+++.B--' + \
                   '--0-----C----9-.++4++++F++++A+.-.'

qr_sample_hex = 'eb3f41586a004889e648c7c701000000' + \
                '48c7c201000000438a04083c00741230' + \
                'd8880648c7c0010000000f0549ffc1eb' + \
                'e648c7c03c00000048c7c7000000000f' + \
                '05e8bcffffffcceef3e4a1c8eff2f5f3' + \
                'f4e2f5e8eeeff2a1d1ede4e0f2e4ada1' + \
                'd2e8f3bb89818192818181818261c1a4' + \
                'a23c7e7da2248182a5858180a5878180' + \
                '1088818190a181868181818180b1c9a7' + \
                '2e288181a5838e258181818d89818186' + \
                'a0898180a5838e20a58581818181818d' + \
                '8d81818381818181dff5f8fea3f8ebfa' + \
                'f1f0edfcfaed81818181818181818181' + \
                '8181818100'

qr_sample_bin = bytes(unhexlify(qr_sample_hex))

instr_sample_0 = bytes([
    0xDF, 0x28,
    0xDF, 0x35, 0x0A, 0x00, 0x00, 0x00,
    0xDF, 0x68, 0x08,
    0xDF, 0x35, 0x0A, 0x00, 0x00, 0x00,
    0xCC,
    0x46, 0x38, 0x88, 0x15, 0x78, 0x56,
    0x54, 0x85, 0x02, 0x54, 0x40, 0x10,
    0x00, 0x11, 0x25, 0x14, 0x92, 0x01,
])

instr_sample_1 = ([
    'xor ecx, ecx',
    'inc ecx',
    'jmp 0x24',
    'pop rsi',
    'mov ebx, dword ptr [rsi]',
    'mov eax, dword ptr [rdi+rcx*4-4]',
    'mov edx, dword ptr [rsi|rcx*4]',
    'mul rdx',
    'div rbx',
    'dec rdx',
    'jne 0x23',
    'inc ecx',
    'cmp ecx, 5',
    'jl 9',
    'ret',
    'hlt',
    'call 6',
], bytes([
    0x67, 0x16, 0xBC, 0x79, 0x7E, 0x9E, 0xDD, 0x77,
    0xF5, 0x98, 0xE8, 0x5C, 0x79, 0x8E, 0x21, 0x72,
    0x22, 0x1B, 0x52, 0x3C, 0x00, 0x00, 0x00, 0x00,
]))

def rax_solver(rbx: int, rdx_list: Sequence[int]) -> Result[list[ModelRef], str]:
    results = []

    v_rax = Int('rax')
    v_rbx = Int('rbx')
    v_rdx = Int('rdx')

    stmt_0 = v_rbx == rbx
    for rdx in rdx_list:
        stmt_1 = v_rdx == rdx
        stmt_2 = ((v_rax * v_rdx) % v_rbx) == 1
        solver = Solver()
        solver.add(stmt_0, stmt_1, stmt_2)

        checked = solver.check()
        if str(checked) != 'sat':
            return Err(f'Solver not satisfied: {checked}')

        model = solver.model()
        results.append(model)

    return Ok(results)

if __name__ == '__main__':
    with section('Remainder'):
        sample_ints = struct.unpack('<IIIII', instr_sample_1[1][:20])
        rbx, rdx_list = sample_ints[0], sample_ints[1:]
        assert rbx == 0x79BC1667
        assert rdx_list == (0x77DD9E7E, 0x5CE898F5, 0x72218E79, 0x3C521B22)
        rax_solved = rax_solver(rbx=rbx, rdx_list=rdx_list).unwrap()
        rax_list = [x[Int('rax')].as_long() for x in rax_solved]
        print(f'RAX values: {[hex(x) for x in rax_list]}')

        rax_bytes = bytes(takewhile(lambda x: x != 0, struct.pack('<IIII', *rax_list)))
        print(f'RAX solved string: "{rax_bytes.decode()}"')

    with section('Brainfuck'):
        print('Brainfuck sample:')
        print(brainfuck_sample)

    with section('QR Code'):
        print('QR sample binary:')
        hexdump.hexdump(qr_sample_bin)
        print()
        print('QR head (disasm):')
        qr_head = qr_sample_bin[:0x43 + 1]
        disasm(Cs(CS_ARCH_X86, CS_MODE_16), qr_head)
        print()
        # QR code head is somewhat-sensible 16-bit intel? Afterwards, seems to be XOR-ed.
        qr_body = bytes([x ^ 0x81 for x in qr_sample_bin[0x43:]])
        # Skip "~~~More Instructions Please, Sir:" string.
        qr_chunk = qr_body[0x21:-1]
        # Extract string from chunk.
        qr_string = qr_chunk[0x54:0x62]
        assert qr_string == b'^ty\x7f"yj{pql}{l'
        print(f'QR Answer: "{bytes([x ^ 0x18 for x in qr_string]).decode()}"')

    with section('Binary Instructions'):
        print('Instructions sample 0 (disasm):')
        disasm(Cs(CS_ARCH_X86, CS_MODE_64), instr_sample_0[:18])
        print()
        print('Instructions sample 0 (data):')
        hexdump.hexdump(instr_sample_0[18:])

    with section('E-mail'):
        print(f'Message: {message}')
