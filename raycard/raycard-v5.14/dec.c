#include <stdio.h>
#include <stdlib.h>

typedef unsigned char u8;

const u8 g_input[15] = {
    0x3C, 0xF7, 0xBF, 0x3C, 0xD9, 0x53, 0x49, 0x57,
    0x33, 0x27, 0x68, 0xBA, 0x70, 0x28, 0x65,
};

void hex_print(const u8 *bytes, size_t len) {
    for (size_t i = 0; i < len; i++) {
        const u8 c = bytes[i];
        if (0x20 <= c && c <= 0x7E) {
            printf("%c", (char)c);
        }
        else {
            printf("\\x%02x", c);
        }
    }
}

void dec(u8 *dest, const u8 *enc, size_t len, u8 key) {
    for (size_t i = 0; i < len; i++) {
        u8 c = (u8)(enc[i] - 3);
        key = (u8)((key * 13) + 37);
        dest[i] = c ^ key;
    }
}

void enc(char *flag, u8 key) {
    while (*flag) {
        key = (u8)((key * 13) + 37);
        *flag ^= key;
        *(flag++) += 3;
    }
}

void perform_dec_all_keys() {
    for (int i = 0; i < 256; i++) {
        u8 buf[15];
        dec(buf, g_input, 15, (u8)i);
        printf("0x%02x: ", (u8)i);
        hex_print(buf, 15);
        printf("\n");
    }
}

void perform_enc_input(const char *flag, u8 key) {
    char buf[64] = {0};
    const size_t len = strlen(flag);
    if (sizeof(buf) <= len) {
        perror("input too long");
        return;
    }
    memcpy(buf, flag, len);
    enc(buf, key);
    hex_print(buf, len);
    printf("\n");
}

int main(int argc, char **argv) {
    if (argc < 2) {
        perform_dec_all_keys();
    } else if (argc == 3) {
        unsigned long key = strtoul(argv[2], NULL, 0);
        perform_enc_input(argv[1], key);
    } else {
        printf("usage: %s [flags-string key]\n", argv[0]);
    }
    return 0;
}
