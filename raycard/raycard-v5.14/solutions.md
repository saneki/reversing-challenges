# Solutions

These are my solutions for the `v5.14` recruiting card challenges. Enjoy! 😏

## Binary Message

```
01000110 01101100 01100001
01100111 00111101 00110001
00110010 01000101 01011010
```

This challenge is just an ASCII string encoded as binary.

Answer: `Flag=12EZ`

## Basic Decryption

```c
void enc(char* flag, byte key)
{
  while(*flag)
  {
    *flag ^= (key = (key*13) + 37);
    *(flag++) += 3;
  }
}
/* Output: 3C F7 BF 3C D9 53 49
57 33 27 68 BA 70 28 65 */
```

This challenge provides a basic encryption function, which takes a `byte` value as a key. The encryption is pretty simple with some addition and XOR-ing, and mutates the key each cycle. Some output bytes are also provided.

To solve this, I reimplemented the `enc` function in C along with a corresponding `dec` function. I then ran the `dec` function with the given output for each possible key value: `[0 .. 255]`

Source: [`dec.c`](dec.c)

Looking through the output, we see the flag text for `key = 0x42`.

Answer: `Flag=Don'tPanic`

## ARM Vector Instructions

```
04105FE2
082BB1EC
544102F3
044B2DED
700020E1
4E47D08B
69176790
70245FB5
3F5252E1
```

This challenge provides some hex, which looks like nine 32-bit integers.

They look kind of like ARM instructions, given the little-endian most-significant-byte `0xE?` pattern. Decoding as ARM reveals several vector instructions.

```
0x0: subs r1, pc, #4
0x4: vldmia r1!, {d2, d3, d4, d5}
0x8: veor q2, q1, q2
0xc: vpush {d4, d5}
0x10: bkpt #0
```

The d-registers and q-registers reference the same register space:

```
q0 == d0:d1
q1 == d2:d3
q2 == d4:d5
...
```

We can see the latter four instructions and following data being loaded into `d2` through `d5`. The first half of this data (instructions) is then XOR-ed by the second half (misc data), by using the q-registers.

Performing this XOR ourselves, we reveal the ASCII flag string.

Answer: `Flag=VectorXORr`

## Intel Code XOR

```
6A 03
59
48 8D 35 06 00 00 00
51
48 AD
48 93
48 AD
48 31 D8
50
E2 F6
F4
--
C8 1B 45 B9
33 89 F6 F4 0E
36
00 2E
05 0D 54 68 2D
09 15 28 5B 32
3D
0B 66 4C
41
47 1D 74 49 43
66
90
```

This challenge begins with what looks like intel instructions, separated into two sections. Decoding the latter section doesn't yield much, but the first section does decode properly.

```
0x0: push 3
0x2: pop rcx
0x3: lea rsi, [rip + 6]
0xa: push rcx
0xb: lodsq rax, qword ptr [rsi]
0xd: xchg rax, rbx
0xf: lodsq rax, qword ptr [rsi]
0x11: xor rax, rbx
0x14: push rax
0x15: loop 0xd
0x17: hlt
```

This sample seems to loop three times. Each loop, it loads the next 64-bit integer from the instruction data itself (starting at offset `0x10`), and XORs that value with the following 64-bit integer. Each resulting value is then pushed to the stack to presumably form a string.

However, these instructions only contain one 64-bit integer starting at offset `0x10`, as the sample is only `0x18` bytes in length. This is where the following section of data comes in.

With both sections concatenated, we may perform this process with four 64-bit integers starting at offset `0x10`, resulting in three 64-bit integers. Together, they form an ASCII string with the flag.

Answer: `Flag=TicketOffTheStack`
