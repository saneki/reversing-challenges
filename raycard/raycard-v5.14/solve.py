#!/usr/bin/env python

from base64 import b64decode
from capstone import Cs, CS_ARCH_ARM, CS_ARCH_X86, CS_MODE_ARM, CS_MODE_64
from contextlib import contextmanager
import hexdump
from itertools import takewhile
from typing import Generator, Iterable, SupportsIndex

def disasm(md: Cs, data: bytes, offset: int = 0):
    for instr in md.disasm(data, offset=offset):
        print('0x%x: %s %s' % (instr.address, instr.mnemonic, instr.op_str))

@contextmanager
def section(title: str, count: int = 40) -> Generator[None, None, None]:
    try:
        print('\n--- ' + title + ' ' + '-' * (count - len(title)) + '\n')
        yield
    finally:
        ...

def xor_bytes(in0: bytes, in1: bytes) -> bytes:
    return bytes([a ^ b for (a, b) in zip(in0, in1)])

def zstr(iter: Iterable[SupportsIndex]) -> str:
    return bytes(takewhile(lambda x: x != 0, iter)).decode()

email = b64decode('Y3RmQHJheXRoZW9uLmNvbQ==').decode()
assert email == 'ctf@raytheon.com'

binary_samples = bytes([
    0b01000110, 0b01101100, 0b01100001,
    0b01100111, 0b00111101, 0b00110001,
    0b00110010, 0b01000101, 0b01011010,
])

hex_samples = bytes([
    0x04, 0x10, 0x5F, 0xE2,
    0x08, 0x2B, 0xB1, 0xEC,
    0x54, 0x41, 0x02, 0xF3,
    0x04, 0x4B, 0x2D, 0xED,
    0x70, 0x00, 0x20, 0xE1,
    0x4E, 0x47, 0xD0, 0x8B,
    0x69, 0x17, 0x67, 0x90,
    0x70, 0x24, 0x5F, 0xB5,
    0x3F, 0x52, 0x52, 0xE1,
])

instr_samples_0 = bytes([
    0x6A, 0x03,
    0x59,
    0x48, 0x8D, 0x35, 0x06, 0x00, 0x00, 0x00,
    0x51,
    0x48, 0xAD,
    0x48, 0x93,
    0x48, 0xAD,
    0x48, 0x31, 0xD8,
    0x50,
    0xE2, 0xF6,
    0xF4,
])

instr_samples_1 = bytes([
    0xC8, 0x1B, 0x45, 0xB9,
    0x33, 0x89, 0xF6, 0xF4, 0x0E,
    0x36,
    0x00, 0x2E,
    0x05, 0x0D, 0x54, 0x68, 0x2D,
    0x09, 0x15, 0x28, 0x5B, 0x32,
    0x3D,
    0x0B, 0x66, 0x4C,
    0x41,
    0x47, 0x1D, 0x74, 0x49, 0x43,
    0x66,
    0x90,
])

rc4_sample = bytes([0xF4, 0x73, 0xF1, 0x30, 0x24, 0xB5, 0x4F, 0x35, 0x58, 0xF1, 0x62, 0x48, 0x55])

if __name__ == '__main__':
    with section('Binary samples'):
        print(f'Decoded: {binary_samples.decode()}')

    with section('Hex samples'):
        disasm(Cs(CS_ARCH_ARM, CS_MODE_ARM), hex_samples[:20])
        print()
        print('Following data:')
        hexdump.hexdump(hex_samples[20:])
        print()

        parts = hex_samples[4:20], hex_samples[20:36]
        result = zstr(xor_bytes(parts[0], parts[1]))
        print(f'Result: {result}')

    with section('Instruction samples (0)'):
        disasm(Cs(CS_ARCH_X86, CS_MODE_64), instr_samples_0)
        print()

        full = instr_samples_0 + instr_samples_1
        parts = full[16:24], full[24:32], full[32:40], full[40:48]
        r0 = xor_bytes(parts[0], parts[1])
        r1 = xor_bytes(r0, parts[2])
        r2 = xor_bytes(r1, parts[3])
        result = zstr(r2 + r1 + r0)
        print(f'Result: {result}')

    with section('E-mail'):
        print(f'Decoded: {email}')
