#!/usr/bin/env bash

sendsleep='0.1'
session='no-dollars'

# Use short & basic PS1
export PROMPT_COMMAND="PS1='\$ '"

tmux new-session -d -s "$session"

# Setup window & panes
tmux split-window -v
tmux split-window -h

# Run pane
tmux select-pane -t 0 && sleep "$sendsleep"
tmux send-keys './solve.sh -r' C-m

# Sleep to let server spawn...
echo '[+] Waiting so server can spawn...'
sleep 1

# Connect pane
tmux select-pane -t 1 && sleep "$sendsleep"
tmux send-keys './solve.sh -c' C-m

# Output pane
tmux select-pane -t 2 && sleep "$sendsleep"
tmux send-keys './solve.sh -o' C-m

# Position on "connect" pane to type commands
tmux select-pane -t 1

tmux attach-session -t "$session"
