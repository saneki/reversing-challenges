#!/usr/bin/expect -f

set input [lindex $argv 0];

set timeout -1
spawn ./no-dollars
expect "Enter a filename to find on your system: "
send -- "$input\n"
expect eof
